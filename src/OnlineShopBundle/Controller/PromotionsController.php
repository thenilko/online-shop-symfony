<?php

namespace OnlineShopBundle\Controller;

use OnlineShopBundle\Entity\Promotion;
use OnlineShopBundle\Form\PromotionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_EDITOR')")
 */

class PromotionsController extends Controller
{
    /**
     * @Route("admin/promotions", name="all_promotions")
     */
    public function allPromotionsView()
    {

        $promotions = $this->getDoctrine()
            ->getRepository(Promotion::class)
            ->findAll();

        return $this->render(
            'admin/promotions/index.html.twig',
            [
                'promotions' => $promotions
            ]
        );
    }

    /**
     * @Route("admin/promotions/add", name="add_promotions_view")
     * @Method("GET")
     *
     */
    public function addPromotionsView()
    {
        $form = $this->createForm(PromotionType::class);
        return $this->render('admin/promotions/add.html.twig',
            [
                'form' => $form->createView(),
                'class_error' => 'no-error'
            ]
        );
    }



    /**
     * @Route("admin/promotions/add", name="add_promotions_action")
     * @Method("POST")
     *
     * @param Request $request
     * @return Response
     */
    public function addProductAction (Request $request)
    {
        $promotion = new Promotion();
        $form = $this->createForm(
            PromotionType::class,
            $promotion
        );
        $form->handleRequest($request);
        if ($form->isValid()) {

            $entityManager = $this->getDoctrine()
                ->getManager();
            $entityManager->persist($promotion);
            $entityManager->flush();

            $this->addFlash("info", "Promotion with name " . $promotion->getName() . " was added successfully");

            return $this->redirectToRoute("all_promotions");
        }

        return $this->render("admin/promotions/add.html.twig",
            [
                'form' => $form->createView(),
                'class_error' => 'has-error'
            ]
        );
    }


    /**
     * @Route("admin/promotion/edit/{id}", name="edit_promotion_view")
     * @Method("GET")
     *
     * @param Promotion $promotion
     * @return Response
     */
    public function editView(Promotion $promotion)
    {
        $form = $this->createForm(
            PromotionType::class,
            $promotion
        );
        return $this->render("admin/promotions/edit.html.twig",
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("admin/promotion/edit/{id}", name="edit_promotion_action")
     * @Method("POST")
     *
     * @param Promotion $promotion
     * @param Request $request
     * @return Response
     */
    public function editAction( Promotion $promotion, Request $request)
    {
        $form = $this->createForm(
            PromotionType::class,
            $promotion
        );

        $form->handleRequest($request);

        if ($form->isValid()) {

            $entityManager = $this->getDoctrine()
                ->getManager();
            $entityManager->persist($promotion);
            $entityManager->flush();

            $this->addFlash("info", "Promotion with name" . $promotion->getName() . " was edited successfully");


            return $this->redirectToRoute("all_promotions");
        }

        return $this->render("admin/promotions/edit.html.twig",
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("admin/promotion/delete/{id}", name="delete_promotion_action")
     * @Method("POST")
     *
     * @param Promotion $promotion
     * @return Response
     */
    public function deleteAction(Promotion $promotion)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($promotion);
        $entityManager->flush();
        $this->addFlash("delete", "Promotion deleted!!!");
        return $this->redirectToRoute("all_promotions");
    }
}
