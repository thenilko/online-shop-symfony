<?php

namespace OnlineShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * Order
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="OnlineShopBundle\Repository\OrderRepository")
 */
class Order
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="order_date", type="datetime")
     */
    private $orderDate;


//    /**
//     * @var array
//     * @ORM\Column(name="ordered_products", type="array")
//     */
//    private $ordered_products;

    /**
     * @var OrderedProduct[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="OnlineShopBundle\Entity\OrderedProduct", mappedBy="order")
     *
     */
    private $ordered_products;

    public function __construct()
    {
        $this->ordered_products = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Order
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }


    /**
     * Set orderDate
     *
     * @param \DateTime $orderDate
     *
     * @return Order
     */
    public function setOrderDate($orderDate)
    {
        $this->orderDate = $orderDate;

        return $this;
    }

    /**
     * Get orderDate
     *
     * @return \DateTime
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * @return OrderedProduct[]|ArrayCollection
     */
    public function getOrderedProducts()
    {
        return $this->ordered_products;
    }

    /**
     * @param Product $ordered_products
     */
    public function setOrderedProducts($ordered_products)
    {
        $this->ordered_products = $ordered_products;
    }
}

