<?php

namespace OnlineShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * OrderedProduct
 *
 * @ORM\Table(name="ordered_products")
 * @ORM\Entity(repositoryClass="OnlineShopBundle\Repository\OrderedProductRepository")
 */
class OrderedProduct
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=11, scale=2)
     */
    private $price;

    /**
     * @var Order[]|ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="OnlineShopBundle\Entity\Order",inversedBy="ordered_products")
     * @ORM\JoinColumn(referencedColumnName="id",name="order_id")
     */
    private $order;

    /**
     * @ManyToOne(targetEntity="OnlineShopBundle\Entity\Product", inversedBy="orderedProducts")
     * @ORM\JoinColumn(name="product_id",referencedColumnName="id")
     */
    private $product;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set price
     *
     * @param string $price
     *
     * @return OrderedProduct
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }
}

