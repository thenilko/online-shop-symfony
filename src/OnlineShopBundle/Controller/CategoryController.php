<?php

namespace OnlineShopBundle\Controller;

use OnlineShopBundle\Entity\Category;
use OnlineShopBundle\Form\CategoryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_EDITOR')")
 */
class CategoryController extends Controller
{
    /**
     * @Route("/admin/categories", name="all_categories")
     */
    public function allCategoriesView()
    {
        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();


        return $this->render(
            'admin/categories/index.html.twig',
            [
                'categories' => $category,

            ]
        );
    }

    /**
     * @Route("/admin/category/add", name="add_category_form")
     * @Method("GET")
     *
     */
    public function addCategoryView()
    {
        $form = $this->createForm(CategoryType::class);
        return $this->render('admin/categories/add.html.twig', [
            'form' => $form->createView(),
            'class_error' => 'no-error'
        ]);
    }


    /**
     * @Route("/admin/category/add", name="add_category_action")
     * @Method("POST")
     *
     * @param Request $request
     * @return Response
     */
    public function addAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm(
            CategoryType::class,
            $category
        );
        $form->handleRequest($request);
        if ($form->isValid()) {
            if ($category->getCategoryImageForm() instanceof UploadedFile) {
                $file = $category->getCategoryImageForm();
                $fileName = $this->get('app.file_uploader')->upload($file);

                $category->setCategoryImage($fileName);
            }


            $entityManager = $this->getDoctrine()
                ->getManager();
            $entityManager->persist($category);
            $entityManager->flush();

            $this->addFlash("info", "Product with name " . $category->getTitle() . " was added successfully");

            return $this->redirectToRoute("all_categories");
        }

        return $this->render("admin/categories/add.html.twig",
            [
                'form' => $form->createView(),
                'class_error' => 'has-error'
            ]
        );
    }


    /**
     * @Route("/admin/category/edit/{id}", name="edit_category_form")
     * @Method("GET")
     *
     * @param Category $category
     * @return Response
     */
    public function editView(Category $category)
    {
        $form = $this->createForm(
            CategoryType::class,
            $category
        );

        return $this->render("admin/categories/edit.html.twig",
            [
                'form' => $form->createView(),
                'category' => $category
            ]
        );
    }

    /**
     * @Route("/admin/category/edit/{id}", name="edit_category_action")
     * @Method("POST")
     *
     * @param Category $category
     * @param Request $request
     * @return Response
     */
    public function editAction(Category $category, Request $request)
    {
        $form = $this->createForm(
            CategoryType::class,
            $category
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($category->getCategoryImageForm() instanceof UploadedFile) {
                $file = $category->getCategoryImageForm();
                $fileName = $this->get('app.file_uploader')->upload($file);

                $category->setCategoryImage($fileName);
            }


            $entityManager = $this->getDoctrine()
                ->getManager();
            $entityManager->persist($category);
            $entityManager->flush();

            $this->addFlash("info", "Category with name " . $category->getTitle() . " was edited successfully");


            return $this->redirectToRoute("all_categories");
        }

        return $this->render("admin/categories/edit.html.twig",
            [
                'form' => $form->createView(),
                'category' => $category
            ]
        );
    }

    /**
     * @Route("/admin/category/delete/{id}", name="delete_category_action")
     * @Method("POST")
     *
     * @param Category $category
     * @return Response
     */
    public function deleteAction(Category $category)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($category);
        $entityManager->flush();
        $this->addFlash("delete", "Category deleted!!!");
        return $this->redirectToRoute("all_categories");
    }
}
