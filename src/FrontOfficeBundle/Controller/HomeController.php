<?php

namespace FrontOfficeBundle\Controller;

use OnlineShopBundle\Entity\Category;
use OnlineShopBundle\Entity\Product;
use OnlineShopBundle\Entity\Promotion;
use OnlineShopBundle\Repository\PromotionRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class HomeController extends Controller
{
    /**
     * @Route("/", name="front_home")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $all_categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        $biggest_promo = $this->getDoctrine()->getRepository(Promotion::class)->fetchBiggestPromotion();

        return $this->render(':frontend:index.html.twig',
            [
                'all_categories' => $all_categories,
                'promo' => $biggest_promo
            ]
        );
    }


    /**
     * @Route("/products", name="fo_all_products")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function productsIndex(Request $request)
    {
        $products = $this->getDoctrine()
            ->getRepository(Product::class)
        ->findAllProductsInStock();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $products, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            6/*limit per page*/
        );

        $calc = $this->get('price_calculator');

        return $this->render('frontend/allproducts.html.twig',
            [
                'pagination' => $pagination,
                'calc' => $calc
            ]
        );
    }


    /**
     * @Route("/products/category/{id}", name="products_category")
     *
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function productsByCategoryAction(Category $category)
    {
        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findProductsByCategoryInStock($category);

        $calc = $this->get('price_calculator');

        return $this->render('frontend/productsbycategory.html.twig',
            [
                'products' => $products,
                'calc' => $calc
            ]
        );
    }


    /**
     * @Route("/product/{id}", name="single_product_view")
     *
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function singleProductView(Product $product)
    {

        $calc = $this->get('price_calculator');
        return $this->render('frontend/singleProduct.html.twig',
            [
              'product' => $product,
                'calc' => $calc
            ]
        );
    }
}
