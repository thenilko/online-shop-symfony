<?php

namespace OnlineShopBundle\Controller;

use OnlineShopBundle\Entity\Order;
use OnlineShopBundle\Entity\Role;
use OnlineShopBundle\Entity\User;
use OnlineShopBundle\Form\UserEditAdminType;
use OnlineShopBundle\Form\UsersType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    /**
     * @Route("/register", name="user_register_form")
     * @Method("GET")
     *
     * @return Response
     */
    public function register()
    {
        $form = $this->createForm(UsersType::class);
        return $this->render('user/register.html.twig',
            [
                'form' => $form->createView(),
                'class_error' => 'no-error'
            ]
        );
    }

    /**
     * @Route("/register", name="user_register_action")
     * @Method("POST")
     *
     * @param Request $request
     * @return Response
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UsersType::class, $user, [
            'validation_groups' => 'registration'
        ]);
        $form->handleRequest($request);

        if($form->isValid()){

            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPasswordRow());
            $user->setPassword($password);


            $userRole = $this->getDoctrine()
                ->getRepository(Role::class)
                ->findBy(['name' => 'ROLE_USER']);
            $user->setRoles($userRole);
            $user->setCash('1000');
            $user->setIsEnabled('1');
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('login');
        }
        return $this->render('user/register.html.twig',
            [
                'form' => $form->createView(),
                'class_error' => 'has-error'
            ]
        );
    }


    /**
     * @Route("/admin/profile", name="user_profile_action")
     *
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_EDITOR')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editProfileAction( Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $orders = $this->getDoctrine()->getRepository(Order::class)->findBy(
            [
                'userId' => $user->getId()
            ]
        );
        $form = $this->createForm(
            UsersType::class,
            $user,
            ['validation_groups' => 'editProfile']
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if($user->getProfileImageRow() instanceof UploadedFile){
                $file = $user->getProfileImageRow();
                $fileName = $this->get('app.file_uploader')->upload($file);

                $user->setProfileImage($fileName);
                /*понеже е за логнатия усер и се нулира за да махнем UploadedFile от USer обетка защото по късно усера се сериализира*/
                $user->setProfileImageRow(null);
            }


            $entityManager = $this->getDoctrine()
                ->getManager();
            $entityManager->persist($user);
            $entityManager->flush();



            return $this->redirectToRoute("user_profile_action");
        }
        return $this->render('user/profile.html.twig',
            [
                'form' => $form->createView(),
                'class_error' => 'has-error',
                'orders' => $orders
            ]
        );
    }


    /**
     * @Route("/users/profile", name="fo_users_profile_action")
     *
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function foUserProfileAction( Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $orders = $this->getDoctrine()->getRepository(Order::class)->findBy(
            [
                'userId' => $user->getId()
            ]
        );

        $form = $this->createForm(
            UsersType::class,
            $user,
            ['validation_groups' => 'editProfile']
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if($user->getProfileImageRow() instanceof UploadedFile){
                $file = $user->getProfileImageRow();
                $fileName = $this->get('app.file_uploader')->upload($file);

                $user->setProfileImage($fileName);
                /*понеже е за логнатия усер и се нулира за да махнем UploadedFile от USer обетка защото по късно усера се сериализира на по-късен етап*/
                $user->setProfileImageRow(null);
            }


            $entityManager = $this->getDoctrine()
                ->getManager();
            $entityManager->persist($user);
            $entityManager->flush();



            return $this->redirectToRoute("fo_users_profile_action");
        }
        return $this->render('frontend/profile.html.twig',
            [
                'form' => $form->createView(),
                'class_error' => 'has-error',
                'orders' => $orders
            ]
        );
    }


    /**
     * @Route("/users", name="all_users")
     *
     * @Security("has_role('ROLE_ADMIN')")
     * @return Response
     */
    public function allUsersView()
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        return $this->render('user/index.html.twig',
            [
                'users' => $users
            ]
        );
    }


    /**
     * @Route("/user/profile/{id}", name="user_profile_admin_action")
     *
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @param User $user
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userProfileAdminAction(User $user ,Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createForm(
            UserEditAdminType::class,
            $user
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {


            if($user->getProfileImageRow() instanceof UploadedFile){
                $file = $user->getProfileImageRow();
                $fileName = $this->get('app.file_uploader')->upload($file);

                $user->setProfileImage($fileName);
            }


            $entityManager = $this->getDoctrine()
                ->getManager();
            $entityManager->persist($user);
            $entityManager->flush();


            $this->addFlash("info", "User with username " . $user->getUsername() . " was edited successfully");
            return $this->redirectToRoute("all_users");
        }
        return $this->render('user/editByAdmin.html.twig',
            [
                'form' => $form->createView(),
                'class_error' => 'has-error',
                'user' => $user
            ]
        );
    }
}
