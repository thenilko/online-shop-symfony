<?php

namespace OnlineShopBundle\Form;

use OnlineShopBundle\Entity\Category;
use OnlineShopBundle\Entity\Promotion;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PromotionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class)
            ->add('dateStart', DateType::class)
            ->add('dateEnd', DateType::class)
            ->add('percentage', TextType::class)
            ->add('category', EntityType::class , array(
                'class'    => 'OnlineShopBundle:Category' ,
                'expanded' => true ,
                'required' => false))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Promotion::class,
        ));
    }

    public function getBlockPrefix()
    {
        return 'online_shop_bundle_promotion_type';
    }
}
