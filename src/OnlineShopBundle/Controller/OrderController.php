<?php

namespace OnlineShopBundle\Controller;

use OnlineShopBundle\Entity\OrderedProduct;
use OnlineShopBundle\Entity\Product;
use OnlineShopBundle\Entity\Order;

use OnlineShopBundle\Form\UsersType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;


class OrderController extends Controller
{
    const DEFAULT_QUANTITY = 1;


    /**
     * @Route("add-to-cart/{id}", name="add_to_cart_action")
     *
     * @param Product $product
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addToCartAction(Product $product , Request $request)
    {

        $aBasket = $product;
        // Get Value from session
        $sessionVal = $this->get('session')->get('aBasket');
        // Append value to retrieved array.
        $sessionVal[] = $aBasket;
        // Set value back to session
        $this->get('session')->set('aBasket', $sessionVal);

        $this->addFlash("addTocart", "The product " . $product->getTitle() . " was added to cart!");
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("cart", name="cart_view")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cartView()
    {
        $session = new Session();
        $itemCart = $session->get('aBasket');

        return $this->render('frontend/cartView.html.twig',
            [
                'itemCart' => $itemCart
            ]
        );
    }

    /**
     * @Route("/remove/{id}", name="remove_item")
     *
     * @param Product $product
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeItemCart(Product $product)
    {
        $session = new Session();
        $aBasket = $session->get('aBasket');
        $product_to_remove = array($product);
        $updatedBasket = array_diff($aBasket,$product_to_remove);
        $session->set('aBasket', $updatedBasket);

        return $this->redirectToRoute('cart_view');
    }


    /**
     *
     * @Route("/checkout", name="checkout_view")
     *
     * @Method("GET")
     */
    public function checkoutView()
    {
        $session = new Session();
        $itemCart = $session->get('aBasket');

        $user = $this->getUser();

        $user_cash = $user->getCash();

        $form = $this->createForm(
            UsersType::class,
            $user
        );

        $subtotal = 0.00;
        foreach($itemCart as $key=>$value)
        {
            $subtotal+= $value->getPrice();
        }

        return $this->render('frontend/checkout.html.twig',
            [
                'form' => $form->createView(),
                'itemCart' => $itemCart,
                'user' => $user,
                'subtotal' => $subtotal,
                'userss' => $user_cash

            ]
        );
    }


    /**
     * @Route("/checkout", name="checkout_action")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function checkoutAction( Request $request)
    {
        $session = new Session();
        $itemCart = $session->get('aBasket');

        $subtotal = 0.00;
        foreach($itemCart as $key=>$value)
        {
            $subtotal+= $value->getPrice();
            $product_q[] = [$value->getId(), $value->getQuantity() - self::DEFAULT_QUANTITY];
        }
        $user = $this->getUser();
        $user_cash = $user->getCash();
        $form = $this->createForm(
            UsersType::class,
            $user,
            [
                'validation_groups' => 'order'
            ]
        );
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            if($subtotal > $user_cash){
                $this->addFlash("info", "You don`t have enough money !");
            }else{


                $order = new Order();
                $order->setUserId($user->getId());
                //$order->setOrderedProducts($itemCart);

                $order->setOrderDate(new \DateTime());
                $cash_after_order = $user->getCash() - $subtotal;
                $user->setCash($cash_after_order);

                $entityManager = $this->getDoctrine()
                    ->getManager();

                $entityManager->persist($order);
                $entityManager->flush();

                foreach($itemCart as $key=>$value)
                {
                    $entityManager = $this->getDoctrine()
                        ->getManager();

                    /** @var Product $product */
                    $product = $value;
                    $product = $entityManager->getRepository(Product::class)->findOneBy(["id"=>$product->getId()]);
                    $product->setQuantity($product->getQuantity()- self::DEFAULT_QUANTITY);

                    $orderProducts = new OrderedProduct();
                    $orderProducts->setPrice($product->getPrice());
                    $orderProducts->setProduct($product);
                    $orderProducts->setOrder($order);

                    $entityManager->persist($product);
                    $entityManager->flush();
                    $entityManager->persist($orderProducts);
                    $entityManager->flush();
                }
                $session->remove('aBasket');

                return $this->redirectToRoute('front_home');
            }
        }

        return $this->render('frontend/checkout.html.twig',
            [
                'form' => $form->createView(),
                'itemCart' => $itemCart,
                'subtotal' => $subtotal
            ]
        );
    }

    /**
     * @Route("/admin/orders", name="all_orders")
     *
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_EDITOR')")
     */

    public function allOrders ()
    {
        $orders = $this->getDoctrine()
            ->getRepository(Order::class)
            ->findAll();


        return $this->render(
            'admin/orders/index.html.twig',
            [
                'orders' => $orders
            ]
        );
    }
}
