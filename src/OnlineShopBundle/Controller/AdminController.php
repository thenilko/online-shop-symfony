<?php

namespace OnlineShopBundle\Controller;

use OnlineShopBundle\Repository\PromotionRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 *
 * Class AdminController
 * @package OnlineShopBundle\Controller
 */
class AdminController extends Controller
{
    /**
     * @Route("/admin/home", name="admin_dashboard")
     */
    public function adminArea()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        if($user = $this->get('security.authorization_checker')->isGranted('ROLE_USER')){
            return $this->redirectToRoute('front_home');
        }else{
            return $this->render('admin/index.html.twig');
        }
    }
}
