/**
 * Created by Danail on 1.4.2017 г..
 */

$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
});

$('#dateStart, #dateEnd').datepicker();
