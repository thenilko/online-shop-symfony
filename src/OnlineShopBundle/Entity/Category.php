<?php

namespace OnlineShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Category
 *
 * @ORM\Table(name="categories")
 * @ORM\Entity(repositoryClass="OnlineShopBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $category_image;


    /**
     * @var string
     */
    private $category_image_form;


    /**
     * @ORM\OneToMany(targetEntity="OnlineShopBundle\Entity\Product", mappedBy="category")
     */
    private $products;


    /**
     * @ORM\OneToMany(targetEntity="OnlineShopBundle\Entity\Promotion", mappedBy="category")
     */
    private $promotions;


    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->promotions = new ArrayCollection();
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Category
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Category
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function __toString()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getCategoryImage()
    {
        return $this->category_image;
    }

    /**
     * @param mixed $category_image
     */
    public function setCategoryImage($category_image)
    {
        $this->category_image = $category_image;
    }

    /**
     * @return mixed
     */
    public function getCategoryImageForm()
    {
        return $this->category_image_form;
    }

    /**
     * @param mixed $category_image_form
     */
    public function setCategoryImageForm($category_image_form)
    {
        $this->category_image_form = $category_image_form;
    }

}

