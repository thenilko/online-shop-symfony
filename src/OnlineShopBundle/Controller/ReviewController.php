<?php

namespace OnlineShopBundle\Controller;

use OnlineShopBundle\Entity\Product;
use OnlineShopBundle\Entity\Review;
use OnlineShopBundle\Form\ReviewType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ReviewController extends Controller
{
    /**
     * @Route("/product/{id}", name="product_reviews")
     *
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewByProduct(Product $product)
    {
        return $this->render(':frontend:singleProduct.html.twig', [
            'product' => $product
        ]);
    }

    /**
     * @Route("/product/{id}/reviews/add", name="leave_review_form")
     * @Method("GET")
     *
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function leaveReviewFormAction(Product $product)
    {
        return $this->render(':frontend:leave_review.html.twig', [
            'reviewForm' => $this->createForm(ReviewType::class)->createView(),
            'product' => $product
        ]);
    }

    /**
     * @Route("/product/{id}/reviews/add", name="leave_review_process")
     * @Method("POST")
     *
     * @param Product $product
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function leaveReviewProcess(Product $product, Request $request)
    {
        $review = new Review();
        $form = $this->createForm(ReviewType::class, $review);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $review->setProduct($product);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($review);
            $entityManager->flush();
            $this->addFlash("info", "Review added");
            return $this->redirectToRoute('product_reviews', ['id' => $product->getId()]);
        }

        return $this->render('frontend/singleProduct.html.twig', [
            'product' => $product
        ]);
    }
}
