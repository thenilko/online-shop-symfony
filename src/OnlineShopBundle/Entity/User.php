<?php

namespace OnlineShopBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 * @UniqueEntity(
 *     fields={"username"},
 *      message="Username: {{ value }}  is already in use."
 * )
 * @UniqueEntity(
 *     fields={"email"},
 *      message="The email: {{ value }} is already in use."
 * )
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="OnlineShopBundle\Repository\UserRepository")
 */
class User implements AdvancedUserInterface
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"registration","editProfile", "order"})
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     */
    private $username;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"registration","editProfile", "order"})
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     *
     * @Assert\NotBlank(groups={"registration","editProfile", "order"})
     * @Assert\Length(
     *      min = 2,
     *      max = 20,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(name="first_name", type="string", length=255, unique=true)
     */
    private $first_name;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"registration","editProfile", "order"})
     * @Assert\Length(
     *      min = 2,
     *      max = 20,
     *      minMessage = "Your last name must be at least {{ limit }} characters long",
     *      maxMessage = "Your last name cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(name="last_name", type="string", length=255, unique=true)
     */
    private $last_name;

    /**
     * @var string
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;


    /**
     *
     * @Assert\NotBlank(groups={"registration"})
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Your password must be at least {{ limit }} characters long",
     *      maxMessage = "Your password cannot be longer than {{ limit }} characters"
     * )
     */
    private $password_row;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $profile_image = null;


    /**
     * @Assert\Image(
     *     mimeTypes={"image/png", "image/jpg", "image/jpeg"},
     *     groups={"editProfile"}
     *     )
     */
    private $profile_image_row;


    /**
     *
     * @Assert\NotBlank(groups={"editProfile", "order"})
     * @ORM\Column(type="string" , nullable=true)
     */
    private $address;


    /**
     * @Assert\NotBlank(groups={"editProfile", "order"})
     * @ORM\Column(type="integer", nullable=true)
     *
     */
    private $phone;

    /**
     * @ORM\Column(type="decimal")
     */
    private $cash;

    /**
     * @var boolean
     * Assert\NotBlank(groups={})
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isEnabled = "true";


    /**
     * @var Role[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="OnlineShopBundle\Entity\Role", inversedBy="users")
     * @ORM\JoinTable(name="user_roles", joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")})
     */
    private $roles;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param string $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    public function getProfileImage()
    {
        return $this->profile_image;
    }

    public function setProfileImage($profile_image)
    {
        $this->profile_image = $profile_image;

        return $this;
    }


    /**
     * @param ArrayCollection|Role[] $roles
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    public function getRoles()
    {
        return array_map(function (Role $r) {
            return $r->getName();
        }, $this->roles->toArray());
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getCash()
    {
        return $this->cash;
    }

    /**
     * @param mixed $cash
     */
    public function setCash($cash)
    {
        $this->cash = $cash;
    }

    /**
     * @return mixed
     */
    public function getPasswordRow()
    {
        return $this->password_row;
    }

    /**
     * @param mixed $password_row
     */
    public function setPasswordRow($password_row)
    {
        $this->password_row = $password_row;
    }

    /**
     * @return mixed
     */
    public function getProfileImageRow()
    {
        return $this->profile_image_row;
    }

    /**
     * @param mixed $profile_image_row
     */
    public function setProfileImageRow($profile_image_row)
    {
        $this->profile_image_row = $profile_image_row;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;
    }

    /**
     * Checks whether the user's account has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return bool true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is locked.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a LockedException and prevent login.
     *
     * @return bool true if the user is not locked, false otherwise
     *
     * @see LockedException
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a CredentialsExpiredException and prevent login.
     *
     * @return bool true if the user's credentials are non expired, false otherwise
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is enabled.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent login.
     *
     * @return bool true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled()
    {
        return $this->getIsEnabled();
    }
}

