<?php

namespace OnlineShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Product
 *
 * @ORM\Table(name="products")
 * @ORM\Entity(repositoryClass="OnlineShopBundle\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"add", "edit"})
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"add", "edit"})
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"add", "edit"})
     * @ORM\Column(name="quantity", type="integer")
     */
    private $quantity = 0;


    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"add", "edit"})
     * @ORM\Column(name="price", type="decimal", precision=11, scale=2)
     */
    private $price;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $product_image;

    /**
     *
     * @Assert\Image(
     *     mimeTypes={"image/png", "image/jpg", "image/jpeg"},
     *      groups= {"add"}
     *  )
     */
    private $image_form;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $position;


    /**
     * @ORM\ManyToOne(targetEntity="OnlineShopBundle\Entity\Category", inversedBy="products")
     */
    private $category;

    /**
     * @var Review[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="OnlineShopBundle\Entity\Review", mappedBy="product")
     */
    private $reviews;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_created;
    /**
     * @ORM\OneToMany(targetEntity="OnlineShopBundle\Entity\OrderedProduct", mappedBy="product")
     */
    private $orderedProducts;

    public function __construct()
    {
        $this->reviews = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    public function getProductImage()
    {
        return $this->product_image;
    }

    public function setProductImage($product_image)
    {
        $this->product_image = $product_image;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param string $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function __toString() {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getImageForm()
    {
        return $this->image_form;
    }

    /**
     * @param mixed $image_form
     */
    public function setImageForm($image_form)
    {
        $this->image_form = $image_form;
    }


    /**
     * @return Review[]|ArrayCollection
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * @param Review[]|ArrayCollection $reviews
     */
    public function setReviews($reviews)
    {
        $this->reviews = $reviews;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param mixed $date_created
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
    }

    /**
     * @return mixed
     */
    public function getOrderedProducts()
    {
        return $this->orderedProducts;
    }

    /**
     * @param mixed $orderedProducts
     */
    public function setOrderedProducts($orderedProducts)
    {
        $this->orderedProducts = $orderedProducts;
    }
}

