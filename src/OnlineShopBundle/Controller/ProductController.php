<?php

namespace OnlineShopBundle\Controller;

use DateTime;
use OnlineShopBundle\Entity\Product;
use OnlineShopBundle\Form\ProductType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_EDITOR')")
 */

class ProductController extends Controller
{
    /**
     * @Route("admin/products", name="all_products")
     */
    public function allProductsView(Request $request)
    {

        $product = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findBy(
                [

                ],
                [
                    'date_created' => 'DESC'
                ]
            );

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $product, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            5/*limit per page*/
        );

        return $this->render(
            'admin/products/index.html.twig',
            [
                'products' => $pagination
            ]
        );
    }

    /**
     * @Route("admin/product/add", name="add_product_view")
     * @Method("GET")
     *
     */
    public function addProductView()
    {
        $form = $this->createForm(ProductType::class);
        return $this->render('admin/products/add.html.twig',
            [
                'form' => $form->createView(),
                'class_error' => 'no-error'
            ]
        );
    }



    /**
     * @Route("admin/product/add", name="add_product_action")
     * @Method("POST")
     *
     * @param Request $request
     * @return Response
     */
    public function addProductAction (Request $request)
    {
        $product = new Product();
        $form = $this->createForm(
            ProductType::class,
            $product,
            [
                'validation_groups' => 'add'
            ]
        );
        $form->handleRequest($request);
        if ($form->isValid()) {
            $file = $product->getImageForm();
            $fileName = $this->get('app.file_uploader')->upload($file);

            $product->setProductImage($fileName);

            $todayDate = new DateTime();
            $product->setDateCreated($todayDate);

            $entityManager = $this->getDoctrine()
                ->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            $this->addFlash("info", "Product with name " . $product->getTitle() . " was added successfully");

            return $this->redirectToRoute("all_products");
        }

        return $this->render("admin/products/add.html.twig",
            [
                'form' => $form->createView(),
                'class_error' => 'has-error'
            ]
        );
    }


    /**
     * @Route("admin/product/edit/{id}", name="edit_product_view")
     * @Method("GET")
     *
     * @param Product $product
     * @return Response
     */
    public function editView(Product $product)
    {
        $form = $this->createForm(
            ProductType::class,
            $product
        );

        $product_image = $product->getProductImage();

        return $this->render("admin/products/edit.html.twig",
            [
                'form' => $form->createView(),
                'product_image' => $product_image
            ]
        );
    }

    /**
     * @Route("admin/product/edit/{id}", name="edit_product_action")
     * @Method("POST")
     *
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    public function editAction( Product $product, Request $request)
    {
        $form = $this->createForm(
            ProductType::class,
            $product,
            [
                'validation_groups' => 'edit'
            ]
        );

        $form->handleRequest($request);

        if ($form->isValid()) {

            if($product->getImageForm() instanceof UploadedFile){
                $file = $product->getImageForm();
                $fileName = $this->get('app.file_uploader')->upload($file);
                $product->setProductImage($fileName);
            }


            $entityManager = $this->getDoctrine()
                ->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            $this->addFlash("info", "Product with name" . $product->getTitle() . " was edited successfully");


            return $this->redirectToRoute("all_products");
        }

        return $this->render("admin/products/edit.html.twig",
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("admin/product/delete/{id}", name="delete_product_action")
     * @Method("POST")
     *
     * @param Product $product
     * @return Response
     */
    public function deleteAction(Product $product)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($product);
        $entityManager->flush();
        $this->addFlash("delete", "Product deleted!!!");
        return $this->redirectToRoute("all_products");
    }

}
